//
//  RNUIKitYuliantoModule.swift
//  RNUIKitYuliantoModule
//
//  Copyright © 2022 Yulianto Saparudin. All rights reserved.
//

import Foundation

@objc(RNUIKitYuliantoModule)
class RNUIKitYuliantoModule: NSObject {
  @objc
  func constantsToExport() -> [AnyHashable : Any]! {
    return ["count": 1]
  }

  @objc
  static func requiresMainQueueSetup() -> Bool {
    return true
  }
}
