import React, { useEffect } from 'react'
import RNUIKitYuliantoModule, { Counter } from 'rn-ui-kit-yulianto'

const App = () => {
  useEffect(() => {
    console.log(RNUIKitYuliantoModule)
  })

  return <Counter label='Greetings' />
}

export default App
